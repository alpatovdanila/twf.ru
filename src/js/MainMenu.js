export class MainMenu {
    /**
     * Controls mainmenu dropdown
     * @param {HTMLElement} element
     */
    constructor(element) {
        this.closeTimeout = null;
        let submenus = [...element.querySelectorAll('.top-menu__subnav-container')];
        submenus.forEach( this.bindHandlers.bind(this));
    }

    bindHandlers(submenu){
        let activator = submenu.parentNode;
        activator.addEventListener('mouseover', this.openSubmenu(submenu));
        activator.addEventListener('mouseleave', this.closeSubmenu(submenu));
    }

    openSubmenu(submenu){
        return event => {
            clearTimeout(this.closeTimeout);
            submenu.classList.add('top-menu__subnav-container_visible');
        }
    }

    closeSubmenu(submenu){
        return event => {
            this.closeTimeout = setTimeout(()=>{
                submenu.classList.remove('top-menu__subnav-container_visible');
            }, 1000)
        }
    }
}