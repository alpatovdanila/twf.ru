export class FormRequirements {
    constructor(formElement) {
        this.display = formElement.querySelector('.form-requirements-left') || null;
        this.submit = formElement.querySelector('[type="submit"]');
        this.required = [...formElement.querySelectorAll('.label.label_required')].map(label => ({
            input: label.parentNode.querySelector(`#${label.htmlFor}`),
            label: label.innerText
        }));

        formElement.addEventListener('input', this.renderEmptyRequired.bind(this))
        formElement.addEventListener('submit', event=>{
            if(!!this.getEmptyRequired(this.required).length) event.preventDefault();
        })
        this.renderEmptyRequired();

    }

    getEmptyRequired(required){
        return required.filter(required => !required.input.value);
    }

    renderEmptyRequired() {
        let empty = this.getEmptyRequired(this.required);
        this.submit.disabled = !!empty.length;
        this.display.innerHTML = '';
        let helper = FormRequirements.getHelper(empty);
        if(!!helper) this.display.appendChild(helper);
    }

    static getHelper(empties) {
        if(!empties.length){
            return null;
        }else{
            const helper = document.createElement('div');
            helper.innerHTML = `<h2>Осталось ввести:</h2><ul></ul>`;
            let list = helper.querySelector('ul');

            empties.forEach(empty => {
                let item = document.createElement('li');
                let link = document.createElement('a');
                link.classList.add('link', 'link_js');

                link.innerHTML = empty.label;
                link.addEventListener('click', ()=>empty.input.focus());
                item.appendChild(link);
                list.appendChild(item);
            });

            return helper;
        }
    }


}