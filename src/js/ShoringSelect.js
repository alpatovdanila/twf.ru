const SHORING_SELECT_DESCRIPTIONS = window['SHORING_SELECT_DESCRIPTIONS'];


export class ShoringSelect {
    constructor(element) {

        let shorings = Array.from(element.querySelectorAll("[data-role='shoring']")).map(element => {
            let type = +element.dataset.type;
            let info = SHORING_SELECT_DESCRIPTIONS.find(desc => desc.type === type);
            if (!info) throw 'No description fond for shoring with type ' + type;
            return {
                element,
                ...info,
            }
        });

        this.selected = shorings.find(({element}) => !!element.dataset.active);
        this.descriptionContainer = element.querySelector('[data-role="description"]');

        shorings.forEach(shoring => {
            shoring.element.addEventListener('mouseover', this.onShoringMouseOver(shoring));
            shoring.element.addEventListener('mouseleave', this.onShoringMouseLeave(shoring));
        })

        if(this.selected) this.onShoringMouseOver(this.selected)();

        console.log(this);
    }

    onShoringMouseOver(shoring) {
        return event => this.setDescription(ShoringSelect.createDescription(shoring));
    }

    onShoringMouseLeave(shoring) {
        return event => this.setDescription(!!this.selected ? ShoringSelect.createDescription(this.selected) : '')
    }

    setDescription(string) {
        this.descriptionContainer.innerHTML = string;
    }

    static createDescription(shoring) {
        return `
            <span class="shoring-select-description__model">${shoring.name}</span>
            <span class="shoring-select-description__separator">►</span> 
            <span class="shoring-select-description__sentence">${shoring.depth}</span> 
            <span class="shoring-select-description__separator">►</span> 
            <span class="shoring-select-description__sentence">${shoring.method}</span> 
        `
    }
}

