import {MainMenu} from "./MainMenu";
import {ShoringSelect} from "./ShoringSelect";
import {FormRequirements} from "./FormRequirements";

// Mainmenu open/close submenus handler
new MainMenu(document.querySelector('.top-menu'));

// Shoring selector on internal pages
let shoringSelector = document.querySelector("[data-component='shoring-select']");
if(!!shoringSelector) new ShoringSelect(shoringSelector);

// Form required summary
[...document.querySelectorAll('form')].forEach( form => new FormRequirements(form));


// Go to top button
document.querySelector("[data-component='topper-button']").addEventListener('click', e => window.scrollTo({top:0}));

